/**
 * Client side code.
 */

//Calling Registration Controller  
 (function() { //this is an IIFE
    "use strict";
    angular.module("AssessmentApp").controller("RegistrationCtrl",RegistrationCtrl); 
    RegistrationCtrl.$inject = ["$http"]; //inject the component

    function RegistrationCtrl($http) { 
        var self = this; // vm

        self.user = { //creating an object call email
            email: "",
            password: "",
            confirmpassword:"",
            fullname:"",
            gender:"",
            dateOfBirth:"",
            address:"",
            nationality:"",
            contactnumber:""
        };

        // Function to capture DOB input and calculate if it meet the minimum age
        self.ageValid = function(dateOfBirth) {    
            // define the min age
            var minAge = 18;
            // calculate the difference between the input date and today's date
            var ageDiff = Date.now() - dateOfBirth.getTime();
            // set the date diff as new date
            var ageDate = new Date(ageDiff); 
            // converting the date into whole number in term of year
            var ageYear = Math.abs(ageDate.getUTCFullYear() - 1970);
            // compare year with minimum age variable
            if(ageYear >= minAge){
                return true;
            } else {
                return false;
            }
        };

        /*
        self.initForm(); */

    //object callback 
        self.displayUser = {
            email: "",
            password: "",
            confirmpassword:"",
            fullname:"",
            gender:"",
            dateOfBirth:"",
            address:"",
            nationality:"",
            contactnumber:""
        };

    // callback results
        self.registerUser = function() {
            console.log(self.user.email);
            console.log(self.user.password);
            console.log(self.user.confirmpassword);
            console.log(self.user.fullname);
            console.log(self.user.gender);
            console.log(self.user.dateOfBirth);
            console.log(self.user.address);
            console.log(self.user.nationality);
            console.log(self.user.contactnumber);
            $http.post("/users", self.user)
                .then(function(result) {
                    console.log(result);
                    self.displayUser.email = result.data.email;
                    self.displayUser.password = result.data.password;
                    self.displayUser.password = result.data.confirmpassword;
                    self.displayUser.name = result.data.fullname;
                    self.displayUser.gender = result.data.gender;
                    self.displayUser.dateOfBirth = result.data.dateOfBirth;
                    self.displayUser.address = result.data.address;
                    self.displayUser.nationality = result.data.nationality;
                    self.displayUser.contactnumber = result.data.contactnumber;
                }).catch(function(e) {
                    console.log(e);
                });
        };
    }

})();