/**
 * Server side code.
 */
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");

var app = express();
app.use(bodyParser.urlencoded({ extended: false })); //turn to false to the input is not complex
app.use(bodyParser.json()); //instruct the express to interpret all the body from browser as json message

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.NODE_PORT || 3001;

//everything that is under client folder will be loaded to communicate wtih the browser
app.use(express.static(__dirname + "/../client/")); 

        app.post("/users", function(req, res) { 
        console.log("Received user object " + req.body);
        console.log("Received user object " + JSON.stringify(req.body));
        var user = req.body;
        console.log("email > " + user.email);
        console.log("password > " + user.password);
        console.log("Full name > " + user.fullname);
        console.log("Gender > " + user.gender);
        console.log("Date Of Birth> " + user.dateOfBirth);
        console.log("Address> " + user.address);
        console.log("Nationality> " + user.nationality);
        console.log("Contact Number> " + user.contactnumber);
        res.status(200).json(user);
    });


app.listen(NODE_PORT, function() {
    console.log("Web App started at " + NODE_PORT);
});

module.exports = app;